# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is the 'room' controller for cornucopia application
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------


@auth.requires_login()
def create():
    """Creates a new room"""
    form = SQLFORM(db.room)

    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    return dict(form=form)


def show():
    """Shows a room"""

    try:
        request.args(0, cast=int)
    except:
        redirect(URL('default', 'index'))

    this_room = db.room(request.args(0, cast=int))

    form = SQLFORM(db.room, this_room).process() if auth.user else None

    this_room_containers = db(db.container.room_id == this_room.id).select()

    return dict(room=this_room,
                containers=this_room_containers,
                form=form)


def search():
    """an ajax wiki search page for cornucopia places"""
    return dict(form=FORM(INPUT(_id='keyword',
                                _name='keyword',
                                _onkeyup="ajax('callback', ['keyword'], 'target');")),
                target_div=DIV(_id='target'))


def callback():
    """an ajax callback that returns a <ul> of links to cornucopia places"""
    query = db.room.name.contains(request.vars.keyword)
    objs = db(query).select(orderby=db.room.name)
    links = [A(o.name, _href=URL('show', args=o.id)) for o in objs]
    return UL(*links)


@auth.requires_login()
def manage():
    """Manage rooms"""
    grid = SQLFORM.smartgrid(db.room)
    return dict(grid=grid)
