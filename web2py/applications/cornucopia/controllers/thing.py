# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is the place controller for cornucopia application
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

@auth.requires_login()
def create():
    """Creates a new thing"""
    form = SQLFORM(db.thing)

    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    return dict(form=form)


def show():
    """Shows a thing"""

    try:
        request.args(0, cast=int)
    except:
        redirect(URL('default', 'index'))

    this_thing = db.thing(request.args(0, cast=int))

    form = SQLFORM(db.thing, this_thing).process() if auth.user else None

#    this_cont_containers = None
#    this_cont_cl = db(db.container_location.container_id == this_cont.id)._select(db.container_location.contained)
#    this_cont_containers = db(db.container.id.belongs(this_cont_cl)).select()
#
#    this_cont_things = db(db.thing.container_id == this_cont.id).select()

    return dict(thing=this_thing,
#                containers=this_cont_containers,
#                things = this_cont_things,
                form=form)

def search():
    """an ajax wiki search page for cornucopia things"""
    return dict(form=FORM(INPUT(_id='keyword',
                                _name='keyword',
                                _onkeyup="ajax('callback', ['keyword'], 'target');")),
                target_div=DIV(_id='target'))


def callback():
    """an ajax callback that returns a <ul> of links to cornucopia things"""
    query = db.thing.name.contains(request.vars.keyword)
    objs = db(query).select(orderby=db.thing.name)
    links = [A(o.name, _href=URL('show', args=o.id)) for o in objs]
    return UL(*links)


def manage():
    """Manage things"""
    # grid = SQLFORM.grid(db.thing)
    grid = SQLFORM.smartgrid(db.thing)
                             # linked_tables = ['room'])
    return dict(grid = grid)
